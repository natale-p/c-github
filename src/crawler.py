# SPDX-License-Identifier: GPL-2.0
# Copyright (c) 2019 Natale Patriciello <natale.patriciello@gmail.com>
import requests
import asyncio
import functools
from html.parser import HTMLParser
import sys

class CrawlError(Exception):
    """
    Exception for any type of error that the crawler can encounter.
    """
    pass

class GitHubRepositorySearchParser(HTMLParser):
    """
    Given the HTML of the GitHub search result, extract the url of each
    repository found, saved as a list in self.hrefs.
    """
    def __init__(self):
        HTMLParser.__init__(self)
        self._is_repo = False
        self.hrefs = []

    def _handle_starttag_in_repo(self, tag, attrs):
        for attr in attrs:
            if attr is not None and attr[0] == "href":
                self.hrefs.append({'url' : attr[1]})

    def handle_starttag(self, tag, attrs):
        if self._is_repo:
            return self._handle_starttag_in_repo (tag, attrs)

        for attr in attrs:
            if attr is not None and attr[0] == "class":
                class_attr = attr[1].split(' ')
                if "repo-list-item" in class_attr:
                    self._is_repo = True

    def handle_endtag(self, tag):
        self._is_repo = False

class GitHubRepositoryParser(HTMLParser):
    """
    Given a GitHub repository page HTML, returns the language statistics,
    save in self.langs as a dictionary.
    """
    def __init__(self):
        HTMLParser.__init__(self)
        self._save_lang = False
        self._save_perc = False
        self._lang_tmp = ""
        self.langs = {}

    def handle_starttag(self, tag, attrs):
        for attr in attrs:
            if attr is not None and attr[0] == "class":
                class_attr = attr[1].split(' ')
                if "lang" in class_attr:
                    self._save_lang = True
                elif "percent" in class_attr:
                    self._save_perc = True

    def handle_endtag(self, tag):
        self._save_lang = False
        self._save_perc = False

    def handle_data(self, data):
        if self._save_lang:
            self._lang_tmp = data
        elif self._save_perc:
            self.langs[self._lang_tmp] = float(data[:-1])

class Crawler:
    """
    Github search result and repository crawler, built around python requests
    but inside an async framework. The methods retrieve_extra_async()
    and retrieve_async() can be called from coroutines, and are
    wrapping the _sync calls with loop.run_in_executor().
    """
    base_url = 'https://github.com/'
    search_url = base_url + 'search/'

    HEADERS = {
    'user-agent': ('Mozilla/5.0 (Macintosh; Intel Mac OS X 10_10_5) '
                   'AppleWebKit/537.36 (KHTML, like Gecko) '
                   'Chrome/45.0.2454.101 Safari/537.36'),
                   }

    def __init__ (self, proxies, keywords, rtype):
        assert (type(proxies) == list)
        assert (type(keywords) == list)
        assert (type(rtype) == str)
        assert (len(proxies) > 0)
        assert (len(keywords) > 0)
        self._proxies = proxies
        self._keywords = keywords
        self._type = rtype

    def _generate_proxy(self, proxy):
        """
        Generate a dictionary of proxies compatible with python requests syntax,
        starting from one proxy passed as a string.
        """
        proxyDict = { }
        if proxy[0:4] == "http":
            proxyDict['http'] = proxy
            proxyDict['https'] = proxy
        else:
            raise CrawlError("The proxy needs the schema before the IP, which should be http")
        return proxyDict

    async def retrieve_extra_async(self, hrefs):
        """
        Given a list of URLs, start retrieving the language stats calling for
        each one _retrieve_extra_async().
        """
        tasks = [asyncio.ensure_future(self._retrieve_extra_async(href)) for href in hrefs]
        result = await asyncio.gather(*tasks)
        return result

    async def _retrieve_extra_async(self, href):
        """
        Async wrapper for _retrieve_extra_sync.
        """
        loop = asyncio.get_event_loop()
        return await loop.run_in_executor(None, self._retrieve_extra_sync, href)

    def _retrieve_extra_sync(self, href):
        """
        Retrieve the language stats through python requests. Passes the response
        to GitHubRepositoryParser() instance. It tries to connect with one
        proxy at time: if the connection succeed, it returns the language
        statistics without trying the other proxies.
        """
        url = self.base_url + href['url']
        for proxy in self._proxies:
            proxyDict = self._generate_proxy(proxy)

            try:
                #response = requests.get(url, headers=self.HEADERS, timeout=1)
                s = requests.Session()
                s.proxies.update(proxyDict)
                response = s.get(url, proxies=proxyDict, timeout=50)
                if response.status_code != 200:
                    raise CrawlError("Malformed URL")
                parser = GitHubRepositoryParser()
                parser.feed(response.text)
                return parser.langs
            except:
                continue
            else:
                break
        return None

    async def retrieve_async(self):
        """
        Async wrapper for _retrieve_sync.
        """
        loop = asyncio.get_event_loop()
        return await loop.run_in_executor(None, self._retrieve_sync)

    def _retrieve_sync(self):
        """
        Retrieve the repository list, generated through a search on GitHub,
        through python requests. Passes the response
        to GitHubRepositorySearchParser() instance. It tries to connect with one
        proxy at time: if the connection succeed, it returns the repository
        list without trying the other proxies.
        """
        url = self.search_url + '?q='
        for keyword in self._keywords:
            url += str(keyword) + '+'
        url = url[:-1] + '&'
        url += 'type=' + self._type;

        for proxy in self._proxies:
            proxyDict = self._generate_proxy(proxy)

            try:
                #response = requests.get(url, headers=self.HEADERS, timeout=1)
                s = requests.Session()
                s.proxies.update(proxyDict)
                response = s.get(url, proxies=proxyDict, timeout=50)
                if response.status_code != 200:
                    raise CrawlError("Malformed URL")
                parser = GitHubRepositorySearchParser ()
                parser.feed(response.text)
                return parser.hrefs
            except Exception as err:
                continue
            else:
                break
        return None
