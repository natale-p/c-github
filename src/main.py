# SPDX-License-Identifier: GPL-2.0
# Copyright (c) 2019 Natale Patriciello <natale.patriciello@gmail.com>

import signal
import sys
import asyncio
import functools
import parser

async def handle_request(queue, reader, writer):
    """
    Read 1000 bytes from the opened connection, and put the data in a queue.
    Writing in that queue will wake up the Parser instance.
    """
    data = await reader.read(1000) # 1Kb limit.. hardcoded
    message = data.decode()
    addr = writer.get_extra_info('peername')

    print(f"Received {message!r} from {addr!r}")
    item = (message, writer)
    await queue.put(item)

async def main():
    """
    Start the server that will listen over the port 60123 until a SIGINT
    or SIGKILL is received.
    """
    loop = asyncio.get_event_loop()

    loop.add_signal_handler(signal.SIGINT, leave)

    queue = asyncio.Queue (loop=loop)
    p = parser.Parser(queue)

    server = await asyncio.start_server(functools.partial(handle_request, queue), '127.0.0.1', 60123)

    addr = server.sockets[0].getsockname()
    print(f'Serving on {addr}, waiting for connections, CTRL-C to stop')

    async with server:
        await asyncio.gather(server.serve_forever(), p.start())

def leave():
    """
    SIGINT handler, stops the event loop and exit
    """
    print("SIGINT received, leaving the program, have a nice day")
    loop = asyncio.get_event_loop()
    loop.stop()
    sys.exit(0)

if __name__ == '__main__':
    asyncio.run(main())
