import parser
import asyncio
import functools


item_replies = [
('{ "keywordsds": [ "openstack", "test"], "proxies": [ "http://194.126.37.94:8080", "http://127.0.0.1:8080"], "type": "Repositories" }', "You sent {'keywordsds': ['openstack', 'test'], 'proxies': ['http://194.126.37.94:8080', 'http://127.0.0.1:8080'], 'type': 'Repositories'}, parse error: keywords key missing\n"),
('{ "keywords": [ "openstack", "test"], "proxxxies": [ "http://194.126.37.94:8080", "http://127.0.0.1:8080"], "type": "Repositories" }', "You sent {'keywords': ['openstack', 'test'], 'proxxxies': ['http://194.126.37.94:8080', 'http://127.0.0.1:8080'], 'type': 'Repositories'}, parse error: proxies key missing\n"),
('{ "keywords": [ "openstack", "test"], "proxies": [ "http://194.126.37.94:8080", "http://127.0.0.1:8080"], "typez": "Repositories" }', "You sent {'keywords': ['openstack', 'test'], 'proxies': ['http://194.126.37.94:8080', 'http://127.0.0.1:8080'], 'typez': 'Repositories'}, parse error: type key missing\n"),
('{ "keywords": "", "proxies": [ "http://194.126.37.94:8080", "http://127.0.0.1:8080"], "type": "Repositories" }', "You sent {'keywords': '', 'proxies': ['http://194.126.37.94:8080', 'http://127.0.0.1:8080'], 'type': 'Repositories'}, parse error: no keyword provided\n"),
('{ "keywords": [ "openstack", "test"], "proxies": "", "type": "Repositories" }', "You sent {'keywords': ['openstack', 'test'], 'proxies': '', 'type': 'Repositories'}, parse error: no proxies provided\n"),
('{ "keywords": [ "openstack", "test"], "proxies": [ "http://194.126.37.94:8080", "127.0.0.1:8080"], "type": "" }', "You sent {'keywords': ['openstack', 'test'], 'proxies': ['http://194.126.37.94:8080', '127.0.0.1:8080'], 'type': ''}, parse error: What is type ?\n"),
('{ "keywords": [ "openstack", "nova", "css" ], "proxies": "22.22.0.1:9000", "type": "Repositories" }', "You sent {'keywords': ['openstack', 'nova', 'css'], 'proxies': ['22.22.0.1:9000'], 'type': 'Repositories'}, The proxy needs the schema before the IP, which should be http\n"),
('{ "keywords": [ "openstack", "nova", "css" ], "proxies": "http://22.22.0.1:9000", "type": "Repositories" }', "You sent {'keywords': ['openstack', 'nova', 'css'], 'proxies': ['http://22.22.0.1:9000'], 'type': 'Repositories'}, Communication problem with GitHub\n"),
('{ "keywords": [ "openstack", "nova", "css" ], "proxies": "http://119.161.98.131:3128", "type": "Repositories" }', '['),
]

class Writer:
    def __init__(self, msg):
        self._data = None
        self._msg = msg

    def write (self, arg):
        self._data = arg
        if self._msg != '[':
            assert (self._data.decode('utf-8') == self._msg)
        else:
            assert (self._data.decode('utf-8')[0] == "[")

    def read(self):
        return self._data;

    async def drain(self):
        pass

    def close(self):
        pass

async def insert_data(queue):
    w = Writer(None)
    item = ("message", w)
    await queue.put(item)

    for item_reply in item_replies:
        w = Writer(item_reply[1])
        item = (item_reply[0], w)
        await queue.put(item)

    await queue.put(None)

async def main():
    loop = asyncio.get_event_loop()
    queue = asyncio.Queue (loop=loop)
    p = parser.Parser(queue)

    await asyncio.gather(p.start(), insert_data(queue))

async def writer_check():
    w = Writer("something")
    w.write("something".encode('utf-8'))
    assert (w.read().decode('utf-8') == "something")
    await w.drain()
    w.close()

def test_parser():
    asyncio.run(main())

def test_writer():
    asyncio.run(writer_check())

if __name__ == '__main__':
    test_parser()
