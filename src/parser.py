# SPDX-License-Identifier: GPL-2.0
# Copyright (c) 2019 Natale Patriciello <natale.patriciello@gmail.com>
import json
import io
import re
from crawler import Crawler

class ParseError(Exception):
    """
    Exception for indicating any parsing error.
    """
    pass

class Parser:
    """
    Wait on a shared queue for incoming data (method start()).
    When the request arrives, the method parse() is in charge of reading the
    input, parsing it, and passing it to a Crawler instance, before returning
    the output to the caller.
    """
    def __init__ (self, queue):
        self.queue = queue

    def _validate(self, data):
        """
        Check that the 'keywords', 'proxies', 'type' keywords are present in
        the input, and perform some length check on the parameters. Raises
        ParseError if something is not as expected.
        """
        if 'keywords' not in data:
            raise ParseError ("keywords key missing")
        elif 'proxies' not in data:
            raise ParseError ("proxies key missing")
        elif 'type' not in data:
            raise ParseError ("type key missing")

        if len(data['keywords']) == 0:
            raise ParseError ("no keyword provided")
        elif len(data['proxies']) == 0:
            raise ParseError ("no proxies provided")
        elif data['type'] != "Repositories":
            raise ParseError (f"What is type {data['type']}?")

    async def start (self):
        """
        Wait on the queue until an input is received.
        """
        while True:
            item = await self.queue.get()
            if item is None:
                break
            await self.parse (item)

    def _get_owner_from(self, href):
        """
        Get the owner of the repo from a link
        """
        return re.search(r'/(.*?)/', href).group(1)

    def _generate_output(self, hrefs, extras):
        """
        Ging the list of repos and the extras of each one, the method
        generates the output that can be converted in JSON and sent through
        the network.
        """
        output = []
        i = 0
        for href in hrefs:
            href['extra'] = {}
            href['extra']['owner'] = self._get_owner_from(href['url'])
            href['extra']['language_stats'] = extras[i]
            href['url'] = Crawler.base_url[:-1] + href['url']
            i = i + 1
            output.append(href)

    async def parse (self, item):
        """
        Process the network input (unvalidated). The crawling job is performed
        by an instance of Crawler.
        """
        # process the item
        print('consuming item {}...'.format(item))

        try:
            data = json.loads(item[0])
            self._validate(data)

            if type(data['proxies']) is not list:
                data['proxies'] = [ data['proxies'] ]

            if type(data['keywords']) is not list:
                data['keywords'] = [ data['keywords'] ]

            c = Crawler(data['proxies'], data['keywords'], data['type'])
            try:
                hrefs = await c.retrieve_async()
                if hrefs is None:
                    raise ParseError("Communication problem with GitHub")

                extra = await c.retrieve_extra_async(hrefs)
                if extra is None:
                    raise ParseError("Communication problem with GitHub")

                output = self._generate_output(hrefs, extra)
                item[1].write((str(json.dumps(hrefs, indent=4)) + '\n').encode('utf-8'))
            except Exception as err:
                if item[1] is not None:
                    item[1].write(f"You sent {data}, {err}\n".encode('utf-8'))

        except json.JSONDecodeError as err:
            # What to do in case of malformed input?
            # I suppose nothing, to avoid DoS.
            pass
        except ParseError as err:
            if item[1] is not None:
                item[1].write(("You sent %s, parse error: %s\n" % (data, str(err))).encode('utf-8'))
        finally:
            if item[1] is not None:
                await item[1].drain()
                item[1].close()
