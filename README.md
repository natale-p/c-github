C-GitHub v0.1
=============

This is a GitHub crawler. It retrieves the list of repositories that matches
a keyword list, and the language statistics of each of them.

## License
See COPYING.

## Dependencies

- Python 3.7
- Python requests
- Python HTMLParser
- pytest

It has been developed on ArchLinux without portability in mind, so probably it
will not work on other platforms. The Makefile is all hardcoded, so please adapt
it to your need.


## Usage

1. Start the server, that will listen on port 60123 until it receives a
SIGINT, using `make run`.
2. Send text requests through a program of your choice (e.g., netcat)

## Server example

```
$ make run
python main.py
Serving on ('127.0.0.1', 60123), waiting for connections, CTRL-C to stop
```

## Client example

```
$ echo '{
  "keywords": [
    "openstack",
    "nova",
    "css"
  ],
  "proxies":
    "http://119.161.98.131:3128",
  "type": "Repositories"
}' | nc 127.0.0.1 60123

(... waiting some seconds for the output being generated...)

[
    {
        "url": "https://github.com/atuldjadhav/DropBox-Cloud-Storage",
        "extra": {
            "owner": "atuldjadhav",
            "language_stats": {
                "CSS": 52.0,
                "JavaScript": 47.2,
                "HTML": 0.8
            }
        }
    },
    {
        "url": "https://github.com/michealbalogun/Horizon-dashboard",
        "extra": {
            "owner": "michealbalogun",
            "language_stats": {
                "Python": 100.0
            }
        }
    }
]
```

## Notes

- GitHub can work only through https, hence it is necessary to use a proxy that
support HTTPS.
- If the proxy does not support HTTPS, an empty result is returned.
- In case of error, a description of what happened should be returned.
- The test checks that for malformed input the response is the error message,
while for a correct input it checks that the program returned an output.
